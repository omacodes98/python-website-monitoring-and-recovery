import time

import requests
import smtplib
import os
import paramiko
import linode_api4
import time
import schedule

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
LINODE_TOKEN = os.environ.get('LINODE_TOKEN')

def monitor_application():
    try:
        response = requests.get('http://143.42.97.120:8080')
        if response.status_code == 200:
            print("Application is running successfully")
        else:
            print("Application Down. Fix it!")
            msg = f'Application returned {response.status_code}'
            send_notification(msg)

            # restart the application

            restart_container()

    except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = "Application not accessible at all. Fix the issue! Restart the application."
        send_notification(msg)
        restart_server_and_container()

def send_notification(email_msg):
    print('sending an email....')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)

def restart_container():
    print('Restarting the application ...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('143.42.97.120', username='root', key_filename='/Users/omabeji/.ssh/id_ed25519')
    stdin, stdout, stderr = ssh.exec_command('docker start c32c7818ce42')
    print(stdout.readlines())
    ssh.close()

def restart_server_and_container():
    # restart linode server
    print("Rebooting the server....")
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, 42379957)
    nginx_server.reboot()

    # restart application
    while True:
        nginx_server = client.load(linode_api4.Instance, 42379957)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break


schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()