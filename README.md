# Python - Website Monitoring and Recovery 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created a server on a cloud platform

* Installed Docker and ran a Docker container on the remote server

* Wrote a Python script that monitors the website by accessing it and validating the HTTP response

* Wrote a Python script that sends an email notification when website is down

* Wrote a Python script that automatically restarts the application & server when the application is down

## Technologies Used

* Python

* Linode 

* Docker 

* Linux 

## Steps 

Step 1: Create a cloud server in my case i created a lionde cloud server 

[Linode server](/images/01_create_a_cloud_server_linode_server.png)

Step 2: Ssh into Linode cloud server 

    ssh root@143.42.97.120

[Ssh into cloud server](/images/02_ssh_into_linode_server.png)

Step 3: Install docker on cloud server 

[Docker installed](/images/03_install_docker_on_cloud_server.png)

Step 4: Run nginx as a docker container on a cloud server 

[Nginx running container](/images/04_04_run_nginx_as_a_docker_container_on_a_cloud_server.png)

Step 5: Access application through browser 

[Application on Browser](/images/05_access_application_through_browser.png)

Step 6: Create a Python file you are going to use to write your python program 

[Python file](/images/06_create_a_python_file_you_are_going_to_use_to_write_your_python_program.png)

Step 7: Use pip to install request 

[installed request](/images/07_use_pip_to_install_request.png)

Step 8: Import request to python file

[Import request](/images/08_import_requests_to_python_file.png)

Step 9: Use get function on request and insert url in a parameter then save response in variable 

[get function](/images/09_use_get_function_on_request_and_insert_url_in_a_parameter_then_save_response_in_variable.png)

Step 10: Print response to check if it was successful 

[print response](/images/10_print_response_to_check_if_it_was_successful.png)

Step 11:  Use .text to get the exact response the browser gets 

[.text](/images/11_to_get_the_exact_response_the_browser_gets.png)

Step 12:  Use status code to get only the code 

[Status code](/images/12_use_status_code_to_get_only_the_code.png)

Step 13: Add conditional logic if else statement which  will tell user when application is up or down on the browser

[Conditional logic](/images/13_add_conditonal_logic_if_else_statement_which_will_tell_user_when_application_is_up_or_down_on_the_browser.png)

Step 14: Import smtplib to python file to enable email alert when application is down 

[Import smtplib](/images/14_import_smtplib_to_python_file_to_enable_email_alerts_when_application_is_down.png)

Step 15: Call SMTP function to configure which email provider you want to use and insert the configuration by putting it in parameters the email provider and the port its accessible on 

[SMTP](/images/15_call_SMTP_function_to_configure_which_email_provider_you_want_to_use_insert_the_configuration_by_putting_it_in_parameters_the_email_provider_and_the_port_its_accessable_on.png)

Step 16: Use with statement which is used with unmanaged resourcees used in exception handling and clean up code to make code cleaner 

[With statement](/images/16_use_with_statement_which_alternative_of_try_finally_statement_which_is_used_with_unmanaged_resources__used_in_exception_hendling_and_clean_up_code_to_make_code_cleaner.png)

Step 17: as smtp mean is that we assign all of what is before it to a variable 

[as](/images/17_what_the_as_smtp_mean_is_that_we_assigning_all_of_what_is_before_it_to_a_variable.png)

Step 18: Call function starttls on smtp which will encrypt the communication from python to our email server

[starttls](/images/18_call_function_starttls_on_smtp_which_will_encrypt_the_communication_from_pytthon_to_our_email_server.png)

Step 19: Call function ehlo which will identify python application with mail server

[ehlo](/images/19_call_function_ehlo_which_will_identify_python_application_with_mail_server.png)

Step 20: Call login function to login into our account 

[login](/images/21_call_login_function_to_login_into_our_account.png)

Step 21: Go on google account and create generated app password 

[Created generated app password](/images/22_go_on_google_account_and_create_generated_app_password.png)

Step 22: In login function put email and password in parameter to enable login

[Login param](/images/23_in_login_function_put_email_and_password_in_parameter_to_enable_login.png)

Step 23: It is not secure to have your password exposed in pyhton file that is going to be pushed to a repo so its better for us to have access to this credentials through environmental vairables set on our local computer, to do this we will need to import os module 

[Import os module](/images/25_import_os_module.png)

Step 24: Call os function environ_get to have access to environmental variables set

[environ_get](/images/26_call_os_function_environ_get_to_have_access_to_environmental_variables_set.png)

Step 25: Set environment variables 

[Environmental varibales](/images/27_set_environment_variables.png)

Step 26: Insert environmental variables to login function parameter for email and password 

[Env  var in param](/images/28_insert_environmental_variable_to_login_function_parameter_for_email_and_password.png)

Step 27: Call sendemail function and put sender, reciever and email message in parameter 

[Sendemail function](/images/29_call_sendemail_function_and_put_sender_reciever_and_email_message_in_parameter.png)

Step 28: Test program by running  and putting if to false to see if else condition is running smoothly 

[Test program](/images/30_test_program_by_running_and_putting_if_to_false_to_see_if_else_condition_is_running_smoothly.png)

[Email sent](/images/31_email_sent.png)

Step 29: Use the try except logic in situations when you dont get any response status code 

[Try Except](/images/32_use_the_try_except_logic_in_situations_when_you_dont_get_any_response%60-status_code.png)

Step 30: Add the email code to the exception block 

[copy email code into exception block](/images/33_add_email_code_to_exception_block.png)

Step 31: Clean up code by creating function to avoid repeating code 

[Create function](/images/34_clean_up_code_by_creating_function_to_avoid_repeating_code.png)

Step 32: Test program to see if it works 

[Test program to see if it works](/images/35_testing_program_it_works.png)

Step 33: Stop container in cloud server 

[Stop container](/images/36_stop_container_in_cloud_server.png)

Step 34: Test program to see if it works fine 

[Test program 1](/images/37_test_program_to_see_if_it_works_fine.png)

[Email sent 2](/images/38_email_sent.png)

Step 35: Instal paramiko with pypi 

[Install paramiko](/images/39_install_paramiko_with_pypi.png)

Step 36: Import paramiko in python file 

[Import paramiko](/images/40_import_paramiko%20.png)

Step 37: After if else statement call function ssh client on paramiko to get the client and save it into a variable 

[SSHClient](/images/41_after_the_else_statement_call_function_ssh_client_on_paramiko_to_get_the_client_and_save_it_into_a_variable.png)

Step 38: Call the connect function on ssh client this is to connect to the cloud server through ssh and add the ip address of the cloud server, the user you want to have access to in the server and the path of your private key 

[connect](/images/42_call_the_connect_function_on_ssh_client_this_is_to_connect_to_the_cloud_server_through_ssh_and_add_the_ip_address_of_the_cloud_server_the_user_you_want_to_have_access_to_in_the_server_and_the_path_of_your_private_key.png)

Step 39: Call function exec command this is to execute commands on the cloud server however we want to see the output of this execute command and we get three output of this execute command stdin, stdout and stderr so save this as variables for this command and print stdin and stdout

[exec](/images/43_call_function_exec_command_this_is_to_execute_commands_on_the_cloud_server_however_we_want_to_see_the_output_of_this_execute_command_and_we_get_three_output_of_this_execute_command_in_out_err_so_save_this_as_variables_for_this_command_and_print_i_o.png)

Step 40: When we first ssh into a erver we get a prompt in which we need to accept by pressing y, because of this we will need the function set missing host key policy and insert auto add policy to the parameter

[set missing host key policy](/images/44_when_we_first_ssh_into_a_server_we_get_a_prompt_in_which_we_need_to_accept_by_pressing_y_because_of_this_we_will_need_the_function_set_missing_hiost_key_policy_and_insert_auto_add_policy_to_the_parameter.png)

Step 41: On standard output call function insert readlines because this is a file and we nee to read the content 

[readfile](/images/45_on_standard_output_call_function_readlines_because_this_is_a_file_and_we_need_to_read_the_contents.png)

Step 42: Test program 

[Test program 3](/images/46_test_program.png)

[Test program 4](/images/47_test_program.png)

Step 43: Close ssh connection by calling function close 

[close](/images/48_close_ssh_connection_by_calling_function_close.png)

Step 44: Now we have tested exec command and have seen the output we can change exec command parameter to docker start container id to restart docker container in situations in which the docker container is having some problems 

[exec change param](/images/49_now_we_have_tested_exec_command_have_seen_the_output_we_can_change_exec_command_parameter_to_docker_start_container_id_to_restart_docker_container_in_situations_in_which_the_docker_container_is_having_some_problems.png)

Step 45: After close function you can print "application restarted" su user knows

[print application restarted](/images/50_after_close_function_you_can_print_application_restarted_so_user_knows.png)

Step 46: Install linode-api4 library to enable communication to linode client 

[Install Linode-api4](/images/51_install_linode-api4_library_to_enable_communication_to_linode_client.png)

Step 47: Import Linode api4 to python file 

[Import Linode-api4](/images/52_import_linode_api4.png)

Step 48: Create Linode token 

[Linode token](/images/53_create_a_linode_token.png)

Step 49: Call linode client function and insert created linode token in parameter and save into a variable 

[Linode client](/images/54_call_linode_client_function_and_insert_created_linode_token_in_parameter_and_save_into_a_variable.png)

Step 50: Call load function to connect to resources on linode and insert instance and its id inside parameter 

[Load function](/images/55_call_load_function_to_connect_to_resources_on_linode_and_insert_Instance_and_its_id_inside_parameter.png)

Step 51: Call reboot function to reboot Linode cloud server
[Reboot function](/images/56_call_reboot_function_to_reboot_linode_cloud_server.png)

Step 52: Go to cloud server and stop running container 

[Stop running container](/images/57_go_to_cloud_server_and_stop_running_container.png)

Step 53: Run program 

[running program](/images/58_run_program.png)

[success](/images/59_success.png)

[success 2](/images/60_success.png)

Step 54: Ater we rebooted the server we want to restart the docker container on the server so we will copy the restart application logic and paste it after server has been rebooted however since we are repeating code its better to put it in a function 

Step 55: Create a function for restarting the container and past the restarting container code inside the function 

[Function for restarting container](/images/62_create_a_function_for_restarting_the_container_and_past_the_restarting_container_code_inside_the_function.png)

Step 56:  Call restart container function where needed in the python program 

[Call resart container](/images/63_call_restart_container_function_where_needed_in_the_python_program.png)

Step 57: Add a logic that will continously loop through until the server goes to running state 

[While loop to check state](/images/64_add_a_logic_that_will_continous_loop_through_until_the_server_goes_to_running_state_will_itrestart_the_container.png)

Step 58: Test program 

[Testing updated program](/images/65_test_program.png)

[Test successful](/images/66_test_success.png)

Step 59: Clean code up by putting the reboot server and container code into a function 

[restart server and container function](/images/67_clean_code_up_by_putting_the_reboot_server_and_container_code_into_a_function.png)

Step 60: Call function in except block 

[function in except block](/images/68_call_function_in_except_block.png)

Step 61: Put the monitoring code into a function

[Monitoring function](/images/69_put_the_monitoring_code_into_a_function.png)

Step 62: Import schedule library 

[Import Schedule](/images/70_import_schedule_library.png)

Step 63: Use the schedule function every to monitor every 5 minutes and insert function in parameter for monitoring application 

[Every](/images/71_use_the_schedule_function_every_to_monitor_every_5_minutes_and_insert_function_in_parameter_for_monitoring_application.png)

Step 64: Run task in loop 

[task in loop](/images/72_run_task_in_loop.png)

Step 65: Test program 

[Final test](/images/73_test_program.png)

## Installation

    brew install python3

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-website-monitoring-and-recovery.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-website-monitoring-and-recovery

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.